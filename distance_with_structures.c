//WAP to find the distance between two points using structures and 4 functions.

#include<stdio.h>
#include<math.h>
struct points
{
  float x, y;
};
 
float length(struct points m, struct points n)
{
  float res;
  res=sqrt((m.x-n.x)*(m.x-n.x)+(m.y-n.y)*(m.y-n.y));
  return res;
}
 
void result(float r)
{
  printf("Distance between 2 points= %f",r);
}
 
int main()
{
  struct points m, n;
  float l=0;
  
 printf("\nEnter The Coordinates of Point A:\n");
 printf("\nX - Axis Coordinate: \t");
 scanf("%f", &m.x);
 printf("\nY - Axis Coordinate: \t");
 scanf("%f", &m.y);
 
 printf("\nEnter The Coordinates of Point B:\n");
 printf("\nX - Axis Coordinate: \t");
 scanf("%f", &n.x);
 printf("\nY - Axis Coordinate:\t");
 scanf("%f", &n.y);
  
 l=length(m, n);
 result(l);
 
 return 0;
}