//Write a program to add two user input numbers using 4 functions.

#include<stdio.h>

int val(int *x);
int sum(int x, int y);
void result(int x, int y, int r);

int main()
{
	int a,b,s=0;
	
	a=val(&a);
	b=val(&b);

	s=sum(a,b);

	result(a,b,s);
}

int val(int *x)
{
	printf("Enter the value=");
	scanf("%d",x);

	return *x;
}

int sum(int x, int y)
{
	int r=0;
	r=x+y;

	return r;
}

void result(int x, int y, int r)
{
	printf("\nSum of %d and %d= %d",x,y,r);
}