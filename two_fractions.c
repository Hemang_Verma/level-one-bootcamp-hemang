//WAP to find the sum of two fractions.

#include<stdio.h>
 
typedef struct
{
   int num;
   int den;
}fraction;
 
fraction input();
fraction sum(fraction a, fraction b);
void res(fraction r);
 
int main()
{
   fraction f1, f2;
 
   printf("\nFirst fraction:-");
   f1=input();
 
   printf("\n\nSecond fraction:-");
   f2=input();
 
   fraction res_fraction=sum(f1,f2);
 
   res(res_fraction);
 
   return 0;
}
 
fraction input()
{
   fraction i;
 
   printf("\n\tEnter the numerator= ");
   scanf("%d",&i.num);
   printf("\n\tEnter the denominator=");
   scanf("%d",&i.den);
 
   return i;
}
 
fraction sum(fraction a,fraction b)
{
   int cd=0, sum=0;
 
   cd=a.den*b.den;
   sum=a.num*b.den+b.num*a.den;
 
   fraction result={sum,cd};
 
   return result;
}
 
void res(fraction r)
{
   printf("\nThe sum of the 2 given fractions= %d/%d",r.num,r.den);
}