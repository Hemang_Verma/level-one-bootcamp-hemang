//Write a program to find the sum of n different numbers using 4 functions

#include<stdio.h>

int main()
{
	int i, n;
  float value, sum=0;
    
  printf("Enter the number of values to be added= ");
  scanf("%d",&n);
  
	for(i=0;i<n;i++)
  {
    printf("Enter a value= ");
    scanf("%f",&value);
    sum+=value;
  }
  
	printf("The sum of %d values=%f",n,sum);
  
	return 0;
}