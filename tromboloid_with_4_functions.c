//WAP to find the volume of a tromboloid using 4 functions.

#include <stdio.h>

float val(float *x);
float vol(float x, float y, float z);
void res(float x, float y, float z, float v);

int main()
{
    float h,d,b,v;
    
    h=val(&h);
    d=val(&d);
    b=val(&b);
    v=vol(h,d,b);
    res(h,d,b,v);
    
    return 0;
}

float val(float *x)
{
  printf("Enter Number:");
  scanf("%f",x);
	
	return *x;
}

float vol(float x, float y, float z)
{
    float vol;
    vol=((x*y*z)+y/z)/3;

    return vol;
}

void res(float a, float b, float c, float vol)
{
  printf("Volume of tromboloid with dimensions of (h=%f,d=%f and b=%f)= %f",a,b,c,vol);
}