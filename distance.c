//WAP to find the distance between two point using 4 functions.

#include <stdio.h>
#include <math.h>
 
float coordinate(float *a);
float calc(float a, float b, float c, float d);
void length(float l);
 
int main()
{
   float r, x1, x2, y1, y2;
  
    printf("\nEnter The Coordinates of Point 1:-\n");
    printf("\nX - Axis Coordinate=");
    x1=coordinate(&x1);
    printf("\nY - Axis Coordinate: \t");
    y1=coordinate(&y1);
  
   printf("\nEnter The Coordinates of Point 2:-\n");
   printf("\nX - Axis Coordinate:\t");
   x2=coordinate(&x2);
   printf("\nY - Axis Coordinate: \t");
   y1=coordinate(&y1);
  
    r=calc(x1, y1, x2, y2);
  
    length(r);
  
    return 0;
}
 
float coordinate(float *a)
{
  printf("\nEnter coordinate= ");
  scanf("%f",a);
 
  return *a;
}
 
float calc(float x1, float x2, float y1, float y2)
{
   float l;
   l=sqrt((x1 - y1) * (x1 - y1) + (x2 - y2) * (x2 - y2));
   return l;
}
 
void length(float l)
{
  printf("\nThe distance between the 2 points= %f",l);
}