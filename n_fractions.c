//WAP to find the sum of n fractions.

#include<stdio.h>

typedef struct Fraction
{
   int num;
   int den;
}fraction;

int number(int n)
{
   printf("Enter the number of fractions to be added=");
   scanf("%d",&n);

   return n;
}

fraction input(int n, fraction a[n])
{
   for(int i=0; i<n; ++i)
   {
      printf("\nFraction %d",i+1);
      printf("\tEnter the numerator=");
      scanf("%d",&a[i].num);
      printf("\tEnter the denominator=");
      scanf("%d",&a[i].den);
   }
}

fraction sum(int n, fraction a[n])
{
   fraction res, temp;
   int i=0, gcd=0, numerator=0, denominator=1;

   for(i=0; i<n; ++i)
   {
      denominator*=a[i].den;
   }

   for(i=0; i<n; ++i)
   {
      numerator+=(a[i].num)*(denominator*a[i].den);
   }

   temp.num=numerator;
   temp.den=denominator;

   for(i=1; i<=temp.num && i<=temp.den; ++i)
   {
      if(temp.num%i==0 && temp.den%i==0)
      {
         gcd=i;
      }
   }

   res.num=temp.num/gcd;
   res.den=temp.num/gcd;

   return res;
}

void result(int n, fraction a[n], fraction res)
{
   int i=0;

   printf("Sum=");

   for(i=0; i<n-1; ++i)
   {
      printf(" %d/%d +",a[i].num,a[i].den);
   }

   printf("%d/%d= %d/%d",a[i].num,a[i].den,res.num,res.den);
}

int main()
{
   int n=0;
   fraction fraction_result, a[n];

   n=number();
   input(n, a);
   fraction_result=sum(n, a);
   result(n, a, fraction_result);

   return 0;
}
